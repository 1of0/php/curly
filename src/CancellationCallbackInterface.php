<?php

namespace OneOfZero\Curly;

/**
 * Interface CancellationCallbackInterface
 *
 * Defines an interface for a cancellation callback.
 */
interface CancellationCallbackInterface
{
    /**
     * Should return true to signal cancellation.
     *
     * @return bool
     */
    public function isCanceled(): bool;
}
