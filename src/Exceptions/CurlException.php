<?php

namespace OneOfZero\Curly\Exceptions;

use RuntimeException;

class CurlException extends RuntimeException
{
}
