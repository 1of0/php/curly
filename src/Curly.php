<?php

namespace OneOfZero\Curly;

use OneOfZero\Curly\Exceptions\CurlException;
use OneOfZero\Curly\Handlers\HandlerInterface;
use OneOfZero\Streams\SharedStreamInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Laminas\Diactoros\Request;
use Laminas\Diactoros\Response;
use Laminas\Diactoros\Stream;

/**
 * Class Curly
 *
 * Object oriented wrapper around cURL with PSR-7 support.
 */
class Curly implements HttpClientInterface
{
    /**
     * Holds the cURL options relevant to this instance.
     *
     * @var CurlyOptions
     */
    private $options;

    /**
     * Holds the configured handler (if any).
     *
     * @var HandlerInterface;
     */
    private $customHandler;

    /**
     * Creates an instance of Curly, optionally pre-configuring it with a CurlyOptions instance.
     *
     * @param CurlyOptions $options
     */
    public function __construct(?CurlyOptions $options = null)
    {
        $this->setOptions($options);
    }

    /**
     * Executes a request with the provided URL and method.
     *
     * Returns a PSR-7 response object.
     *
     * @param string $url
     * @param string $method
     *
     * @return ResponseInterface
     */
    public function requestByUrl(string $url, string $method = 'GET'): ResponseInterface
    {
        return $this->request(new Request($url, $method));
    }

    /**
     * Executes a request with the provided PSR-7 request object.
     *
     * Returns a PSR-7 response object.
     *
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function request(RequestInterface $request): ResponseInterface
    {
        $options = clone $this->options;

        $this->applyRequest($request, $options);

        if ($this->customHandler !== null) {
            $this->customHandler->registerCallbacks($options);
        }

        $headerStream = $this->prepareHeaderStream($options);
        $responseStream = $this->prepareResponseStream($options);

        $channel = curl_init();

        $options->apply($channel);

        $result = curl_exec($channel);

        if ($result === false) {
            throw new CurlException(curl_error($channel), curl_errno($channel));
        }

        if ($responseStream !== null && $responseStream->isSeekable()) {
            $responseStream->rewind();
        }

        $status = curl_getinfo($channel, CURLINFO_HTTP_CODE);
        return new Response($responseStream ?: 'php://temp', $status, $this->parseHeaderStream($headerStream));
    }

    /**
     * Prepares a stream to store headers in.
     *
     * @param CurlyOptions $options
     *
     * @return Stream|null
     */
    private function prepareHeaderStream(CurlyOptions $options): ?Stream
    {
        if ($options->onHeader !== null) {
            return null;
        }

        if ($options->outputHeaderStream === null) {
            $stream = fopen('php://temp', 'r+b');
            $options->outputHeaderStream = $stream;
            return new Stream($stream);
        }

        return new Stream($options->outputHeaderStream);
    }

    /**
     * Prepares a stream to store the response in.
     *
     * @param CurlyOptions $options
     *
     * @return Stream|null
     */
    private function prepareResponseStream(CurlyOptions $options): ?Stream
    {
        if ($options->onWrite !== null) {
            return null;
        }

        if ($options->outputStream === null) {
            $stream = fopen('php://temp', 'r+b');
            $options->outputStream = $stream;
            return new Stream($stream);
        }

        return new Stream($options->outputStream);
    }

    /**
     * Configures the provided CurlyOptions instance to represent the provided PSR-7 request.
     *
     * @param RequestInterface $request
     * @param CurlyOptions $options
     */
    private function applyRequest(RequestInterface $request, CurlyOptions $options): void
    {
        $options->url = strval($request->getUri());
        $options->method = strtoupper($request->getMethod());

        if ($request->getUri()->getPort() !== null) {
            $options->port = $request->getUri()->getPort();
        }

        if ($request->getProtocolVersion() === '1.0') {
            $options->httpVersion = CURL_HTTP_VERSION_1_0;
        }

        if ($request->getProtocolVersion() === '1.1') {
            $options->httpVersion = CURL_HTTP_VERSION_1_1;
        }

        $options->headers = $this->getFormattedHeaders($request);

        $stream = $request->getBody();
        if ($stream !== null) {
            $options->upload = true;
            $options->expectedInputSize = $stream->getSize();
            $options->inputStream = ($stream instanceof SharedStreamInterface)
                ? $stream->getResource()
                : $stream->detach();
        }
    }

    /**
     * Returns the headers from the provided PSR-7 request as a string array of formatted headers.
     *
     * @param RequestInterface $request
     *
     * @return string[]
     */
    private function getFormattedHeaders(RequestInterface $request): array
    {
        $parsedHeaders = [];
        foreach ($request->getHeaders() as $headerName => $headerValues) {
            foreach ($headerValues as $headerValue) {
                $parsedHeaders[] = sprintf('%s: %s', $headerName, $headerValue);
            }
        }
        return $parsedHeaders;
    }

    /**
     * Parses the headers from the provided header stream, and returns them as an array compatible with the PSR-7
     * response object.
     *
     * @param StreamInterface $headerStream
     *
     * @return array
     */
    private function parseHeaderStream(StreamInterface $headerStream = null): array
    {
        if ($headerStream === null) {
            return [];
        }

        $headerLines = explode("\r\n", strval($headerStream));
        $headerStream->rewind();

        $headerMap = [];
        $headers = [];
        foreach ($headerLines as $line) {
            if (strpos($line, ':') === false) {
                continue;
            }

            [$name, $value] = explode(':', $line, 2);
            $normalizedName = strtolower($name);
            $value = trim($value, "\t ");

            if (!array_key_exists($normalizedName, $headerMap)) {
                $headerMap[$normalizedName] = $name;
            }
            $name = $headerMap[$normalizedName];

            if (!array_key_exists($name, $headerMap)) {
                $headers[$name] = [];
            }
            $headers[$name][] = $value;
        }
        return $headers;
    }

    /**
     * Gets the CurlyOptions for this instance.
     *
     * @return CurlyOptions
     */
    public function getOptions(): CurlyOptions
    {
        return $this->options;
    }

    /**
     * Sets the provided CurlyOptions for this instance.
     *
     * @param CurlyOptions $options
     */
    public function setOptions(?CurlyOptions $options = null): void
    {
        if ($options === null) {
            $options = new CurlyOptions();
        }
        $this->options = $options;
    }

    /**
     * Gets the custom handler for this instance (if any).
     *
     * @return HandlerInterface
     */
    public function getCustomHandler(): ?HandlerInterface
    {
        return $this->customHandler;
    }

    /**
     * Sets the custom handler for this instance.
     *
     * @param HandlerInterface $customHandler
     */
    public function setCustomHandler(HandlerInterface $customHandler): void
    {
        $this->customHandler = $customHandler;
    }
}
