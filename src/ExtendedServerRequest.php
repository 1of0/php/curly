<?php

namespace OneOfZero\Curly;

use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\Uri;

if (!defined('JSON_THROW_ON_ERROR')) {
    define('JSON_THROW_ON_ERROR', 0);
}

class ExtendedServerRequest extends ServerRequest
{
    /**
     * Returns a copy of this request with the provided URI pieces as URI.
     *
     * @param string ...$pieces
     *
     * @return static
     */
    public function withUriString(...$pieces): self
    {
        // TODO: Fix this properly - https://tools.ietf.org/html/rfc3986#section-5.2
        $pieces = array_map(static function($item) { return trim($item, '/'); }, $pieces);

        /** @var self $request */
        $request = $this->withUri(new Uri(implode('/', $pieces)));
        return $request;
    }

    /**
     * Returns a copy of this request with the provided string as body.
     *
     * @param string $content
     *
     * @return static
     */
    public function withStringBody(string $content): self
    {
        $stream = new SharedStream('php://temp', 'r+b');
        BinarySafe::write($stream, $content);
        $stream->rewind();

        return $this->withBody($stream);
    }

    /**
     * Returns a copy of this request with the provided form JSON serialized as the request body.
     *
     * @param array $form
     * @param bool $sortFieldsByName
     *
     * @return static
     */
    public function withJsonForm(array $form, bool $sortFieldsByName = false): self
    {
        if ($sortFieldsByName) {
            ksort($form);
        }

        return $this->withJsonString(json_encode($form, JSON_THROW_ON_ERROR));
    }

    /**
     * Returns a copy of this request with the provided object JSON serialized as the request body.
     *
     * @param object $object
     *
     * @return static
     */
    public function withJsonObject($object): self
    {
        return $this->withJsonString(json_encode($object, JSON_THROW_ON_ERROR));
    }

    /**
     * Returns a copy of this request with the provided JSON as the request body.
     *
     * @param string $json
     *
     * @return static
     */
    public function withJsonString(string $json): self
    {
        return $this
            ->withHeader('Content-Type', 'application/json')
            ->withStringBody($json);
    }

    /**
     * Returns a copy of this request with the provided form serialized as the request body.
     *
     * @param array $form
     * @param bool $sortFieldsByName
     *
     * @return static
     */
    public function withUrlEncodedForm(array $form, bool $sortFieldsByName = false): self
    {
        if ($sortFieldsByName) {
            ksort($form);
        }

        $pairs = [];
        foreach ($form as $field => $value) {
            $pairs[] = urlencode($field) . '=' . urlencode($value);
        }
        $buffer = implode('&', $pairs);

        return $this
            ->withHeader('Content-Type', 'application/x-www-form-urlencoded')
            ->withStringBody($buffer);
    }
}
