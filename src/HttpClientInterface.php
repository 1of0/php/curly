<?php

namespace OneOfZero\Curly;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface HttpClientInterface
{
    /**
     * Executes a request with the provided URL and method.
     *
     * Returns a PSR-7 response object.
     *
     * @param string $url
     * @param string $method
     *
     * @return ResponseInterface
     */
    public function requestByUrl(string $url, string $method = 'GET'): ResponseInterface;

    /**
     * Executes a request with the provided PSR-7 request object.
     *
     * Returns a PSR-7 response object.
     *
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function request(RequestInterface $request): ResponseInterface;
}
