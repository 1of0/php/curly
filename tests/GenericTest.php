<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\Curly;
use OneOfZero\Curly\ExtendedServerRequest;

class GenericTest extends AbstractTestCase
{
    public function testSanity(): void
    {
        $this->assertTrue(true);
    }

    public function testGet(): void
    {
        $curly = new Curly();

        $request = $this->buildRequest('GET', 'get');
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson(strval($response->getBody()));
    }

    public function testPost(): void
    {
        $curly = new Curly();

        $request = $this->buildRequest('POST', 'post')
            ->withUrlEncodedForm([ 'keyA' => 'valueA', 'keyB' => 'valueB' ])
        ;
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());

        $responseBody = strval($response->getBody());
        $this->assertJson($responseBody);

        $responseObject = json_decode($responseBody);
        $this->assertArrayHasKey('keyA', (array)$responseObject->form);
        $this->assertArrayHasKey('keyB', (array)$responseObject->form);
    }

    public function testPut(): void
    {
        $curly = new Curly();

        $request = $this->buildRequest('PUT', 'put')
            ->withJsonForm([ 'keyA' => 'valueA', 'keyB' => 'valueB' ]);
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());

        $responseBody = strval($response->getBody());
        $this->assertJson($responseBody);

        $responseObject = json_decode($responseBody);
        $this->assertArrayHasKey('keyA', (array)$responseObject->json);
        $this->assertArrayHasKey('keyB', (array)$responseObject->json);
    }

    public function testNotFound(): void
    {
        $curly = new Curly();

        $request = $this->buildRequest('GET', 'status', 404);
        $response = $curly->request($request);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEmpty(strval($response->getBody()));
    }

    public function testUriBuilder(): void
    {
        $curly = new Curly();

        $request = (new ExtendedServerRequest)
            ->withMethod('GET')
            ->withUriString($this->getBaseUri(), 'get');
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson(strval($response->getBody()));
    }
}
