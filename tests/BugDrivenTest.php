<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\Curly;

class BugDrivenTest extends AbstractTestCase
{
    public function testColonsInHeaderValue(): void
    {
        $curly = new Curly();

        $encodedHeader = urlencode('X-Test') . '=' . urlencode('https://example.com');

        $request = $this->buildRequest('GET', 'response-headers?' . $encodedHeader);
        $response = $curly->request($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('https://example.com', $response->getHeaderLine('X-Test'));
    }
}
