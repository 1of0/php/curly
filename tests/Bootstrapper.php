<?php

namespace OneOfZero\Curly\Tests;

class Bootstrapper
{
    /**
     * @var Bootstrapper $instance
     */
    private static $instance;

    /**
     * @return Bootstrapper
     */
    public static function get()
    {
        if (!self::$instance)
        {
            self::$instance = new Bootstrapper();
        }
        return self::$instance;
    }

    /**
     * @var int $port
     */
    private $port;

    /**
     * @var int $pid
     */
    private $pid;

    /**
     *
     */
    public function __construct()
    {
        self::$instance = $this;

        $pipes = [];
        $this->port = intval(shell_exec('((netstat  -atn | awk \'{printf "%s\n%s\n", $4, $4}\' | grep -oE \'[0-9]*$\'; seq 32768 61000) | sort -n | uniq -u | head -n 1) 2> /dev/null'));

        $process = proc_open(
            "gunicorn3 httpbin:app -b \"127.0.0.1:{$this->port}\"",
            [],
            $pipes
        );
        $this->pid = intval(proc_get_status($process)['pid']);
        sleep(2);
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->killProcessAndChildren($this->pid);
    }

    /**
     * @param int $pid
     */
    private function killProcessAndChildren($pid)
    {
        $children = `ps -ef| awk '\$3 == '$pid' { print  \$2 }'`;
        foreach (explode("\n", $children) as $child)
        {
            if (!is_numeric($child) || $child === $pid)
            {
                continue;
            }
            $this->killProcessAndChildren($child);
        }
        posix_kill($pid, 9);
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }
}

Bootstrapper::get();
