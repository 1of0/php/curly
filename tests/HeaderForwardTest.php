<?php

namespace OneOfZero\Curly\Tests;

use OneOfZero\Curly\Curly;
use OneOfZero\Curly\Handlers\StreamHandler;
use RuntimeException;

class HeaderForwardTest extends AbstractTestCase
{
    /**
     * Checks whether output streams work properly when configured through a StreamHandler.
     */
    public function testHeaderForwarding(): void
    {
        $curly = new Curly();

        $handler = new StreamHandler();
        $handler->enableOutputHeaderForwarding();
        $curly->setCustomHandler($handler);

        $request = $this->buildRequest('GET', 'get');

        $this->expectException(RuntimeException::class);

        $response = $curly->request($request);
        $this->assertEquals(200, $response->getStatusCode());

        // TODO: Abstract header() call in StreamHandler class and test the result
    }
}
