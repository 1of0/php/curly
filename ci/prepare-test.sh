#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install git libzip-dev zip curl libffi-dev python3-pip python3-dev gunicorn3 -yqq

pip3 install --upgrade cffi
pip3 install httpbin

pecl install xdebug-2.9.3

docker-php-ext-install zip
docker-php-ext-enable xdebug

curl --location --output /usr/local/bin/composer https://getcomposer.org/composer-stable.phar
chmod +x /usr/local/bin/composer
